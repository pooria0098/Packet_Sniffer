import struct


class UDP:

    def __init__(self, raw_data):
        self.src_port, self.dest_port, self.size ,self.CheckSum= struct.unpack('! H H H H', raw_data[:8])
        self.data = raw_data[8:]
