import struct


class DNS:

    def __init__(self, raw_data):
        self.TransactionID, self.Flags, self.Question, self.Answer, self.Authority, self.Additional = struct.unpack(
            '! H H H H H H ', raw_data[:12])
        if self.Question == 1:
            self.Query_Type, self.Query_Class = struct.unpack('! H H', raw_data[28:32])
            self.data = raw_data[32:]
        if self.Answer >= 1:
            self.Answer_Name, self.Answer_Type, self.Answer_Class, self.Answer_ttl, self.Answer_length = struct.unpack(
                '! H H H L H', raw_data[32:44])
            self.data = raw_data[44:]
