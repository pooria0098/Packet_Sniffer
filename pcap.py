import struct  # use for packing and unpacking
import time

# pcap is an application programming interface (API) for capturing network traffic

# pcap header Structure

# GLOBAL HEADER
#   magic number
#   major version number
#   minor version number
#   GMT to local correction
#   accuracy of timestamps
#   max length of captured packets,  in bytes  here its 65535 (0xffff) which is default value for tcpdump and wireshark
#   data link type  *0x1 which indicates that the link-layer protocol is Ethernet. Full list:
#   http://www.tcpdump.org/linktypes.html

# Packet header
#   timestamp seconds --- *This is the number of seconds since the start of 1970, also known as Unix Epoch
#   timestamp microseconds --- *microseconds part of the time at which the packet was captured
#   number of octets of packet saved in file contains the size of the saved packet data in our file in bytes (following the header)
#   actual length of packet --- *Both fields' value is same here, but these may have different values in cases where we set the maximum packet length (whose value is 65535 in the global header of our file) to a smaller size
class Pcap:

    def __init__(self, filename, link_type=1):
        self.pcap_file = open(filename, 'wb')
        # struct.pack(format, v1, v2, ...)
        self.pcap_file.write(struct.pack('@ I H H i I I I', 0xa1b2c3d4, 2, 4, 0, 0, 65535, link_type))

    def write(self, data):
        ts_sec, ts_usec = map(int, str(time.time()).split('.'))
        length = len(data)
        self.pcap_file.write(struct.pack('@ I I I I', ts_sec, ts_usec, length, length))
        self.pcap_file.write(data)

    def close(self):
        self.pcap_file.close()
