import socket
from general import *
from arp import ARP
from ethernet import Ethernet
from ipv4 import IPv4
from icmp import ICMP
from tcp import TCP
from udp import UDP
from pcap import Pcap
from http import HTTP
from dns import DNS

TAB_1 = '\t - '
TAB_2 = '\t\t - '
TAB_3 = '\t\t\t - '
TAB_4 = '\t\t\t\t - '

DATA_TAB_1 = '\t   '
DATA_TAB_2 = '\t\t   '
DATA_TAB_3 = '\t\t\t   '
DATA_TAB_4 = '\t\t\t\t   '


def main():
    pcap = Pcap('capture.pcap')
    # socket(AF_PACKET,RAW_SOCKET,...) means L2 socket , Data-link Layer Protocol= Ethernet
    conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))

    while True:
        raw_data, addr = conn.recvfrom(65535)
        pcap.write(raw_data)

        eth = Ethernet(raw_data)

        # Ethernet
        print('\nEthernet Frame:')
        print(TAB_1 + 'Destination: {}, Source: {}, Protocol: {}'.format(eth.dest_mac, eth.src_mac, eth.proto))

        # IPv4
        if eth.proto == 0x0800:
            ipv4 = IPv4(eth.data)
            print(TAB_1 + 'IPv4 Packet:')
            print(TAB_2 + 'Version: {}, Header Length: {}, TypeOfService: {}, TotalLength: {}'.format(ipv4.version,
                                                                                                      ipv4.header_length,
                                                                                                      ipv4.type_of_service,
                                                                                                      ipv4.total_length))
            print(TAB_2 + 'Flags: {}, TTL: {}, Identification: {}, CheckSum: {}'.format(ipv4.flags, ipv4.ttl,
                                                                                        ipv4.Identification,
                                                                                        ipv4.checksum))
            print(TAB_2 + 'Protocol: {}, Source: {}, Destination: {}'.format(ipv4.proto, ipv4.src, ipv4.target))

            # ICMP
            if ipv4.proto == 1:
                icmp = ICMP(ipv4.data)
                print(TAB_1 + 'ICMP Packet:')
                print(TAB_2 + 'Type: {}, Code: {}, Checksum: {}'.format(icmp.type, icmp.code, icmp.checksum))
                print(TAB_2 + 'ICMP Data:')
                print(format_multi_line(DATA_TAB_3, icmp.data))

            # TCP
            elif ipv4.proto == 6:
                tcp = TCP(ipv4.data)
                print(TAB_1 + 'TCP Segment:')
                print(TAB_2 + 'Source Port: {}, Destination Port: {}'.format(tcp.src_port, tcp.dest_port))
                print(TAB_2 + 'Sequence: {}, Acknowledgment: {}'.format(tcp.sequence, tcp.acknowledgment))
                print(TAB_2 + 'Flags:')
                print(TAB_3 + 'URG: {}, ACK: {}, PSH: {}'.format(tcp.flag_urg, tcp.flag_ack, tcp.flag_psh))
                print(TAB_3 + 'RST: {}, SYN: {}, FIN:{}'.format(tcp.flag_rst, tcp.flag_syn, tcp.flag_fin))
                print(TAB_2 + 'Window Size: {}, CheckSum: {}, Urgent pointer: {}'.format(tcp.Windows_size, tcp.CheckSum,
                                                                                         tcp.Urgent_pointer))

                if len(tcp.data) > 0:
                    # HTTP
                    if tcp.src_port == 80 or tcp.dest_port == 80:
                        print(TAB_2 + 'HTTP Data:')
                        try:
                            http = HTTP(tcp.data)
                            http_info = str(http.data).split('\n')
                            for line in http_info:
                                print(DATA_TAB_3 + str(line))
                        except:
                            print(format_multi_line(DATA_TAB_3, tcp.data))

                    # DNS
                    elif tcp.src_port == 53 or tcp.dest_port == 53:
                        print(TAB_2 + 'DNS Protocol:')
                        try:
                            dns = DNS(tcp.data)
                            print(TAB_3 + 'TransactionID: {}, Flags: {}'.format(dns.TransactionID, dns.Flags))
                            print(TAB_3 + 'Question: {}, Answer: {}, Authority: {}, Additional: {}'.format(dns.Question,
                                                                                                           dns.Answer,
                                                                                                           dns.Authority,
                                                                                                           dns.Additional))
                            if dns.Question == 1:
                                print(TAB_3 +'Question_Query : ')
                                print(TAB_4 + 'Query_Type: {}, Query_Class: {}'.format(dns.Query_Type, dns.Query_Class))
                            if dns.Answer >= 1:
                                print(TAB_3 +'Answer_Query : ')
                                print(
                                    TAB_4 + 'Answer_Name: {}, Answer_Type: {}, Answer_Class: {}'.format(dns.Answer_Name,
                                                                                                        dns.Answer_Type,
                                                                                                        dns.Answer_Class))
                                print(TAB_4 + 'Answer_ttl: {}, Answer_length: {}'.format(dns.Answer_ttl,
                                                                                         dns.Answer_length))

                        except:
                            print(format_multi_line(DATA_TAB_3, tcp.data))
                    else:
                        print(TAB_2 + 'TCP Data:')
                        print(format_multi_line(DATA_TAB_3, tcp.data))

            # UDP
            elif ipv4.proto == 17:
                udp = UDP(ipv4.data)
                print(TAB_1 + 'UDP Segment:')
                print(TAB_2 + 'Source Port: {}, Destination Port: {}, Length: {}, CheckSum: {}'.format(udp.src_port,
                                                                                                     udp.dest_port,
                                                                                                     udp.size,
                                                                                                     udp.CheckSum))

                # DNS
                if udp.src_port == 53 or udp.dest_port == 53:
                    print(TAB_2 + 'DNS Protocol:')
                    dns = DNS(udp.data)
                    print(TAB_3 + 'TransactionID: {}, Flags: {}'.format(dns.TransactionID, dns.Flags))
                    print(TAB_3 + 'Question: {}, Answer: {}, Authority: {}, Additional: {}'.format(dns.Question,
                                                                                                   dns.Answer,
                                                                                                   dns.Authority,
                                                                                                   dns.Additional))
                    if dns.Question == 1:
                        print(TAB_3 +'Question_Query : ')
                        print(TAB_4 + 'Query_Type: {}, Query_Class: {}'.format(dns.Query_Type, dns.Query_Class))
                    if dns.Answer == 1:
                        print(TAB_3 +'Answer_Query : ')
                        print(
                            TAB_4 + 'Answer_Name: {}, Answer_Type: {}, Answer_Class: {}'.format(dns.Answer_Name,
                                                                                                dns.Answer_Type,
                                                                                                dns.Answer_Class))
                        print(TAB_4 + 'Answer_ttl: {}, Answer_length: {}'.format(dns.Answer_ttl,
                                                                                 dns.Answer_length))

            # Other IPv4
            else:
                print(TAB_1 + 'Other IPv4 Data:')
                print(format_multi_line(DATA_TAB_2, ipv4.data))

        # ARP
        elif eth.proto == 0x0806:
            arp = ARP(eth.data)
            print(TAB_1 + 'ARP Packet:')
            print(
                TAB_2 + 'Hardware_Type: {}, Protocol_Type: {}, Hardware_Size: {}, Protocol_Size: {}, Opcode: {}'.format(
                    arp.hardware_type, arp.protocol_type,
                    arp.hardware_size, arp.protocol_size,
                    arp.Opcode))
            print(
                TAB_2 + 'src_mac_addr: {}, src_ip_addr: {}, dst_mac_addr: {}, dst_ip_addr" {}'.format(arp.src_mac_addr,
                                                                                                      arp.src_ip_addr,
                                                                                                      arp.dst_mac_addr,
                                                                                                      arp.dst_ip_addr))
            print(TAB_1 + 'ARP Data:')
            print(format_multi_line(DATA_TAB_2, arp.data))

        else:
            print('Other Ethernet Data:')
            print(format_multi_line(DATA_TAB_1, eth.data))

    pcap.close()


main()
